var appAlmacenes = {};
            appAlmacenes.Almacen = Backbone.Model.extend({
                defaults: { descripcion: "", activo: false },
                idAttribute: 'idAlmacen',
                urlRoot: '/ganadmin-2017/almacenes',
                initialize: function(){
                    console.log('Inicializando objeto almacén');
                },
                validate: function () {
                    console.log('Validando almacén');
                }
            });
            appAlmacenes.listaAlmacenes = Backbone.Collection.extend({
                model: appAlmacenes.Almacen,
                url: '/ganadmin-2017/almacenes'
            });
            appAlmacenes.despliegueAlmacenes = new appAlmacenes.listaAlmacenes();
            appAlmacenes.verAlmacen = Backbone.View.extend({
                tagName: 'tr',
                template: _.template($('#template-almacen').html()),
                initialize: function(){
                    console.log('verAlmacen.initialize');
                    this.listenTo(this.model, 'change', this.render);
                },
                render: function(){
                    console.log('verAlmacen.render');
                    this.$el.html(this.template(this.model.toJSON()));
                    return this;
                }
            });
            appAlmacenes.verAlmacenList = Backbone.View.extend({
                el: '#almacenes',
                events: {
                    'click .cmd-delete' : 'borrarAlmacen',
                    'click .cmd-edit' : 'editarAlmacen',
                    'click #cmd-guardar' : 'guardarAlmacen',
                    'click #inp-limpiar' : 'limpiar',
                    'change #inp-busqueda' : 'buscar'
                },
                initialize: function(){
                    appAlmacenes.despliegueAlmacenes.on('add', this.addOne, this);
                    appAlmacenes.despliegueAlmacenes.on('reset', this.addAll, this);
                    appAlmacenes.despliegueAlmacenes.fetch();
                },
                addOne: function(alm){
                    console.log(alm);
                    var vista = new appAlmacenes.verAlmacen({model: alm});
                    $('#lista-almacenes').append(vista.render().el);
                },
                addAll: function(){
                    this.$('#lista-almacenes').html('');                    
                    appAlmacenes.despliegueAlmacenes.each(this.addOne, this);
                },
                buscar: function(){
                    var listFilter = appAlmacenes.despliegueAlmacenes.fetch({data: {descripcion:$('#inp-busqueda').val()}});
                    console.log($('#inp-busqueda').val());
                    appAlmacenes.despliegueAlmacenes.reset(listFilter);
                },
                limpiar: function(){
                    console.log("Limpiar");
                    $('#inp-idAlmacen').val('');
                    $('#inp-descripcion').val('');
                    $('#inp-activo')[0].checked = true;
                    $('#cmd-guardar').text("Guardar");
                },
                editarAlmacen: function(event){
                    console.log("Editar almacén");
                    var idAlmacen = event.currentTarget.id;
                    $('#inp-idAlmacen').val(idAlmacen);
                    var almacen = appAlmacenes.despliegueAlmacenes.get(idAlmacen);
                    $('#inp-descripcion').val(almacen.get('descripcion'));
                    $('#cmd-guardar').text("Actualizar");
                },
                guardarAlmacen: function(){
                    console.log('Guardar');
                    var _this = this;
                    var almacen = {};
                    switch($('#cmd-guardar').text()){
                        case "Guardar":
                            console.log("Guardando un nuevo registro");
                            almacen = new appAlmacenes.Almacen();
                        break;                        
                        case "Actualizar":
                            console.log("Actualizando un registro");
                            almacen = appAlmacenes.despliegueAlmacenes.get($('#inp-idAlmacen').val());
                            almacen.set('idAlmacen', $('#inp-idAlmacen').val());
                        break;
                    }
                    almacen.set('descripcion', $('#inp-descripcion').val());
                    almacen.set('activo', $('#inp-activo')[0].checked);
                    almacen.save({}, {
                        success: function(model, response){
                            console.log("Información almacenada");
                            appAlmacenes.despliegueAlmacenes.add(model);
                            _this.mostrarMensaje('Artículo almacenado con éxito');
                        },
                        error: function(model, response){
                            _this.mostrarMensaje('El artículo no se pudo guardar');
                        }
                    });
                    this.limpiar();
                },
                borrarAlmacen: function(event){
                    console.log('borrarAlmacen');
                    var idAlmacen = event.currentTarget.id;
                    var _this = this;
                    console.log($(event.currentTarget));
                    if (confirm('¿Desea borrar este almacén?')) {
                        var alm = new appAlmacenes.Almacen({ "idAlmacen" : idAlmacen });
                        console.log(alm);
                        alm.destroy({
                           success: function(model, response){
                               console.log("SI SE PUDO");
                               console.log(response);
                               _this.mostrarMensaje('El almacén se eliminó');
                               appAlmacenes.despliegueAlmacenes.remove(idAlmacen);
                               $(event.currentTarget).closest('tr').remove();
                           },
                           error: function(model, response){
                               console.log("NO SE PUDO");
                               console.log(response);
                               _this.mostrarError('El almacén no pudo eliminarse');
                           }
                        });
                    }
                },
                mostrarMensaje: function(mnsj, claseCSS) {
                    claseCss = claseCSS || '';
                    $('#notificacion').text(mnsj);
                    $('#notificacion').addClass(claseCSS);
                    $('#notificacion').show('slow');
                    setTimeout(function() {
                        $('#notificacion').hide('slow');
                        $('#notificacion').text('');
                        $('#notificacion').removeClass(claseCSS);
                    }, 4000);
                },
                mostrarError: function(mnsj) {
                    this.mostrarMensaje(mnsj, 'error');
                }                
            });
            appAlmacenes.vistaAlmacenes = new appAlmacenes.verAlmacenList();