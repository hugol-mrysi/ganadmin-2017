CREATE TABLE Almacen ( 
	id integer NOT NULL GENERATED BY DEFAULT AS IDENTITY(START WITH 1, INCREMENT BY 1),
	descripcion String NOT NULL,
	activo bit
);
