package com.ganadmin.ganadmin.dao;

import com.ganadmin.ganadmin.entidades.DetalleOrdenCompra;
import java.util.List;

public interface DetalleOrdenCompraDAO {
    List<DetalleOrdenCompra> list(Integer idOrdenCompra);
    DetalleOrdenCompra show(Integer id);
    DetalleOrdenCompra add(DetalleOrdenCompra almacen);
    DetalleOrdenCompra update(DetalleOrdenCompra almacen);
    boolean delete(Integer idDetalleOrdenCompra);
}
