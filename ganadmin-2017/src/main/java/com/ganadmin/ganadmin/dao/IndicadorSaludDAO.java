package com.ganadmin.ganadmin.dao;

import com.ganadmin.ganadmin.entidades.IndicadorSalud;
import java.util.List;

public interface IndicadorSaludDAO {
    List<IndicadorSalud> list();
    IndicadorSalud show(Integer idAnimal);
    IndicadorSalud add(IndicadorSalud indicadorSalud);
    IndicadorSalud update(Integer idIndicadorSalud);
    boolean delete(Integer idIndicadorSalud);
}
