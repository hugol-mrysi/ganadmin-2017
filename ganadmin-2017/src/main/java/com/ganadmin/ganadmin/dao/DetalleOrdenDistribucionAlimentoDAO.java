package com.ganadmin.ganadmin.dao;

import com.ganadmin.ganadmin.entidades.DetalleOrdenDistribucionAlimento;
import java.util.List;

public interface DetalleOrdenDistribucionAlimentoDAO {
    List<DetalleOrdenDistribucionAlimento> list(Integer idDetalleOrdenDistribucionAlimento);
    DetalleOrdenDistribucionAlimento show(Integer id);
    DetalleOrdenDistribucionAlimento add(DetalleOrdenDistribucionAlimento almacen);
    DetalleOrdenDistribucionAlimento update(DetalleOrdenDistribucionAlimento almacen);
    boolean delete(Integer idDetalleOrdenDistribucionAlimento);
}
