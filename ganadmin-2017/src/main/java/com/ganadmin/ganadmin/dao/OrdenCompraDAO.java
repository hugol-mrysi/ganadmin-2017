package com.ganadmin.ganadmin.dao;

import com.ganadmin.ganadmin.entidades.OrdenCompra;
import java.util.Date;
import java.util.List;

public interface OrdenCompraDAO {
    List<OrdenCompra> list();
    OrdenCompra show(Integer id);
    OrdenCompra add(OrdenCompra ordenCompra);
    OrdenCompra update(Integer idOrdenCompra);
    List<OrdenCompra> findByFecha(Date fecha);
    List<OrdenCompra> findByRangoFechas(Date fechaInicio, Date fechaFin);
}
