package com.ganadmin.ganadmin.dao;

import com.ganadmin.ganadmin.entidades.Jaula;
import java.util.List;

public interface JaulaDAO {
    List<Jaula> list();
    Jaula show(Integer id);
    Jaula add(Jaula jaula);
    Jaula update(Integer idJaula);
    void activate(Integer idJaula);
    void desactivate(Integer idJaula);
    Jaula findByJaula(Integer idJaula);
    List<Jaula> findByStatusActivate();
}
