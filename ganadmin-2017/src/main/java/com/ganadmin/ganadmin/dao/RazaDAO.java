package com.ganadmin.ganadmin.dao;

import com.ganadmin.ganadmin.entidades.Raza;
import java.util.List;

public interface RazaDAO {
    List<Raza> list();
    Raza show(Integer id);
    Raza add(Raza raza);
    Raza update(Integer idRaza);
    boolean delete(Integer idRaza);
    List<Raza> findByName(String name);
}
