package com.ganadmin.ganadmin.dao;

import com.ganadmin.ganadmin.entidades.Usuario;
import java.util.List;

public interface UsuarioDAO {
    List<Usuario> list();
    Usuario show(Integer id);
    Usuario add(Usuario usuario);
    Usuario update(Integer idUsuario);
    void activate(Integer idUsuario);
    void desactivate(Integer idUsuario);
    List<Usuario> findByName(String name);
    List<Usuario> findByNameStatusActivate(String name);
    List<Usuario> findByStatusActivate();
}
