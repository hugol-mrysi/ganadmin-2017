package com.ganadmin.ganadmin.dao;

import com.ganadmin.ganadmin.entidades.Almacen;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface AlmacenDAO extends JpaRepository<Almacen, Integer> {

    @Query(value="SELECT a FROM Almacen a WHERE a.descripcion LIKE %?1%")
    List<Almacen> buscarPorDescripcionAndActivos(String descripcion);    
}
