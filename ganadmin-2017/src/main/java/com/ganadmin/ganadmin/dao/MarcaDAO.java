package com.ganadmin.ganadmin.dao;

import com.ganadmin.ganadmin.entidades.Marca;
import java.util.List;

public interface MarcaDAO {
    List<Marca> list();
    Marca show(Integer id);
    Marca add(Marca marca);
    Marca update(Integer idMarca);
    boolean delete(Integer idMarca);
    List<Marca> findByName(String name);
}
