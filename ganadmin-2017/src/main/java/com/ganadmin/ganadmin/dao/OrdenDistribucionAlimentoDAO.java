package com.ganadmin.ganadmin.dao;

import com.ganadmin.ganadmin.entidades.OrdenDistribucionAlimento;
import java.util.Date;
import java.util.List;

public interface OrdenDistribucionAlimentoDAO {
    List<OrdenDistribucionAlimento> list();
    OrdenDistribucionAlimento show(Integer id);
    OrdenDistribucionAlimento add(OrdenDistribucionAlimento OrdenDistribucionAlimento);
    OrdenDistribucionAlimento update(Integer idOrdenDistribucionAlimento);
    List<OrdenDistribucionAlimento> findByFecha(Date fecha);
    List<OrdenDistribucionAlimento> findByRangoFechas(Date fechaInicio, Date fechaFin);
}
