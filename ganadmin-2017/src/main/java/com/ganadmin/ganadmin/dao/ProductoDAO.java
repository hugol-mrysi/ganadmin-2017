package com.ganadmin.ganadmin.dao;

import com.ganadmin.ganadmin.entidades.Producto;
import java.util.List;

public interface ProductoDAO {
    List<Producto> list();
    Producto show(Integer id);
    Producto add(Producto producto);
    Producto update(Integer idProducto);
    boolean delete(Integer idProducto);
    List<Producto> findByName(String name);
}
