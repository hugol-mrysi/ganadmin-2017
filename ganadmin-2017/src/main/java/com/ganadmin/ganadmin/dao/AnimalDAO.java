package com.ganadmin.ganadmin.dao;

import com.ganadmin.ganadmin.entidades.Animal;
import java.util.List;

public interface AnimalDAO {
    List<Animal> list();
    Animal show(Integer id);
    Animal add(Animal animal);
    Animal update(Integer idAnimal);
    void activate(Integer idAnimal);
    void desactivate(Integer idAnimal);
    List<Animal> findByName(String name);
    List<Animal> findByNameStatusActivate(String name);
    List<Animal> findByStatusActivate();
}
