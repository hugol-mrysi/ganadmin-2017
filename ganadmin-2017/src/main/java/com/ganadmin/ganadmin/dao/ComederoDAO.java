package com.ganadmin.ganadmin.dao;

import com.ganadmin.ganadmin.entidades.Comedero;
import java.util.List;

public interface ComederoDAO {
    List<Comedero> list();
    Comedero show(Integer id);
    Comedero add(Comedero almacen);
    Comedero update(Comedero almacen);
    void activate(Integer idAlmacen);
    void desactivate(Integer idAlmacen);
    List<Comedero> findByName(String name);
    List<Comedero> findByNameStatusActivate(String name);
    List<Comedero> findByStatusActivate();
}
