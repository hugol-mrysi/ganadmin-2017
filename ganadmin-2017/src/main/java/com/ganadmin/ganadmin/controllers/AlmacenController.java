package com.ganadmin.ganadmin.controllers;

import com.ganadmin.ganadmin.dao.AlmacenDAO;
import com.ganadmin.ganadmin.entidades.Almacen;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/almacenes")
public class AlmacenController {
    
    @Autowired
    AlmacenDAO almacenDAO;
    
    private static Logger logger = LoggerFactory.getLogger(AlmacenController.class);
    
    @RequestMapping(value="", method = RequestMethod.GET)
    public List<Almacen> listAlmacen(){
        logger.debug("Controller@almacenes");
        System.out.println("**** Estoy cargando la lista de almacenes ****");
        return almacenDAO.findAll();
    }
    
    @RequestMapping(value="/idAlmacen", method = RequestMethod.GET)
    public Almacen show(@PathVariable Integer idAlmacen){
        return almacenDAO.findOne(idAlmacen);
    }
    
    @RequestMapping(value = "", params = {"descripcion"}, method = RequestMethod.GET)
    public List<Almacen> buscarDescripcionActivo(@RequestParam("descripcion") String descripcion){
        logger.debug("Controller@almacenes?"+descripcion);
        System.out.println("Controller@almacenes?"+descripcion);
        return almacenDAO.buscarPorDescripcionAndActivos(descripcion);
    }
    
    @RequestMapping(value="/{id}", method= RequestMethod.PUT)
    public Almacen update(@RequestBody Almacen almacen){
        return almacenDAO.save(almacen);
    }
    
    @RequestMapping(value="", method= RequestMethod.POST)
    public Almacen save(@RequestBody Almacen almacen){
        return almacenDAO.save(almacen);
    }
    
    @RequestMapping(value="/{idAlmacen}", method = RequestMethod.DELETE)
    public Almacen delete(@PathVariable Integer idAlmacen){
        Almacen almacen = show(idAlmacen);        
        almacenDAO.delete(almacen);
        return almacen;
    }
}
