package com.ganadmin.ganadmin.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

@Configuration
public class ConfiguracionDataSource {
    
    @Bean(destroyMethod="close")
    @Profile({"desarrollo", "produccion"})
    public DataSource dataSource(){
        try{
            HikariConfig hc = new HikariConfig();
            hc.setDriverClassName("com.mysql.jdbc.Driver");
            hc.setJdbcUrl("jdbc:mysql://localhost:3306/Ganadmin");
            hc.setUsername("root");
            hc.setPassword("root");
            return new HikariDataSource(hc);
            
        } catch(Exception e){
            Logger.getLogger(ConfiguracionDataSource.class.getName()).log(Level.SEVERE, null, e);
            return null;
        }
    }
    
    @Bean
    @Profile("pruebas")
    public DataSource getMockDataSource() {
        return new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.DERBY)
                .addScript("db/estructura.sql")
                .addScript("db/pruebas-datos.sql")
                .build();
    }
}
