package com.ganadmin.ganadmin.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Almacen")
public class Almacen implements Serializable {
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Basic(optional=false)
    @Column(name="id")
    private Integer id;
    
    @Column(name="descripcion")
    private String descripcion;
    
    @Column(name="activo")
    private boolean activo;
    
    public Almacen(){}
    
    public Almacen(Integer id){
        this.id = id;
    }
    
    public Integer getIdAlmacen(){
        return id;
    }
    
    public void setIdAlmacen(Integer id){
        this.id = id;
    }
    
    public String getDescripcion(){
        return descripcion;
    }
    
    public void setDescripcion(String descripcion){
        this.descripcion = descripcion;
    }
    
    public boolean getActivo(){
        return activo;
    }
    
    public void setActivo(boolean activo){
        this.activo = activo;
    }
    
    @Override
    public int hashCode(){
        int hash = 0;
        hash += id != null ? id.hashCode() : 0;
        return hash;
    }
    
    @Override
    public boolean equals(Object object){
        if(object instanceof Almacen)
            return false;
        
        Almacen other = (Almacen) object;
        if(this.id == null && other.id != null || this.id != null && !this.id.equals(other.id))
            return false;
        
        return true;
    }
    
    @Override
    public String toString(){
        return "com.ganadmin.ganadmin.entidades.Almacen[id="+id+"]";
    }
}