import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class arreglosFloralesTest {
    
    public arreglosFloralesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testCalcularPersonasTiempoParaArreglos() {
        System.out.println("calcularPersonasTiempoParaArreglos");
        int n = 0;
        arreglosFlorales instance = new arreglosFlorales();
        int[] result = instance.calcularPersonasTiempoParaArreglos(n);
        assertNull("El resultado es nulo", result);
        if(result == null)
            fail("El método devuelve nulo");
    }
    
    @Test
    public void testValidaCantidadArreglos() {
        System.out.println("testValidaCantidadArreglos");
        int n = 0;
        assertNotSame("No es válido el valor 0", 0, n);
    }
    
    @Test
    public void testValidaPersonas19Arreglos(){
        System.out.println("testValida19Arreglos");
        int n=19;
        arreglosFlorales instance = new arreglosFlorales();
        int[] result = instance.calcularPersonasTiempoParaArreglos(n);
        assertEquals("El resultado es correcto", 1, result[0]);
        if(result == null)
            fail("El método devuelve nulo");
    }
    
    @Test
    public void testValidaTiempo19Arreglos(){
        System.out.println("testValida19Arreglos");
        int n=19;
        arreglosFlorales instance = new arreglosFlorales();
        int[] result = instance.calcularPersonasTiempoParaArreglos(n);
        assertEquals("El resultado es correcto", 285, result[1]);
        if(result == null)
            fail("El método devuelve nulo");
    }
    
    @Test
    public void testValidaPersonas49Arreglos(){
        System.out.println("testValidaPersonas49Arreglos");
        int n=49;
        arreglosFlorales instance = new arreglosFlorales();
        int[] result = instance.calcularPersonasTiempoParaArreglos(n);
        assertEquals("El resultado es correcto", 2, result[0]);
        if(result == null)
            fail("El método devuelve nulo");
    }
    
    @Test
    public void testValidaTiempo49Arreglos(){
        System.out.println("testValidaPersonas49Arreglos");
        int n=49;
        arreglosFlorales instance = new arreglosFlorales();
        int[] result = instance.calcularPersonasTiempoParaArreglos(n);
        assertEquals("El resultado es correcto", 735, result[1]);
        if(result == null)
            fail("El método devuelve nulo");
    }
}
