public class arreglosFlorales {
    public int[] calcularPersonasTiempoParaArreglos(int n)
    {
        if(n == 0) 
            return null;
        
        int time = 15 * n, basePerson = 1, person = 0;
        
        if(n<50){
            if(n<20)
                person = basePerson;
            
            if(n>19 && n<50)
                person = basePerson +1;
        } else
           person = (n / 25) + basePerson;
           
        return new int[]{person, time};
    }
    
    public static void main(String[] args) {
        int cantidadArreglos = 75;
        int[] resultado = new arreglosFlorales().calcularPersonasTiempoParaArreglos(cantidadArreglos);
        System.out.println("Por cada "+ cantidadArreglos +" arreglos se requieren:\n"+
                resultado[0]+" persona(s) y "+resultado[1]+" minutos de preparación");
    }
}
